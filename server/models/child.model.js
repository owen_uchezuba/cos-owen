const{Schema,model} = require('mongoose')

const score = new Schema({
    level:{type: String},
    Score:{type: Number}
})

const child = new Schema({
    username:{type: String, required:true},
    password:{type: String, required:true},
    email:{type:String,required:true},
    level:[score]
})

module.exports = model('child',child)