const {Schema,model} = require('mongoose')



const quiz = new Schema({
    question: {type:String},
    option1: {type: String},
    option2: {type: String},
    option3: {type: String},
    option4: {type: String},
    answer: {type:String},
    decrip: {type:String}   
})

const level = new Schema({
    platform:{type: String, required:true},
    description: {type: String, required:true},
    quiz: [quiz],    
})

module.exports = model('quiz',level)