const{GraphQLList,GraphQLString,GraphQLObjectType,GraphQLSchema,GraphQLNonNull, GraphQLInt} = require('graphql');

const quiz = require('../models/quiz.model')
const child = require('../models/child.model')

const levels = new GraphQLObjectType({
    name: 'levels',
    fields:()=>({    
                level:{type:GraphQLString},
                score:{type:GraphQLInt}          
    })
})

const all = new GraphQLObjectType({
    name:'alllevels',
    fields: ()=>({
        level:{type: GraphQLString},
        description:{type: GraphQLString}
    })
})

const quiz_list = new GraphQLObjectType({
    name:'quiz_list',
    fields: ()=>({
        question: {type: GraphQLString},
        option1: {type: GraphQLString},
        option2: {type: GraphQLString},
        option3: {type: GraphQLString},
        option4: {type: GraphQLString},
        answer: {type:GraphQLString},
        decrip: {type:GraphQLString}
    })
})


const RootQueryType = new GraphQLObjectType({
    name:'RootQueryType',
    fields:()=>({
        login:{
            type: GraphQLString,
            args: {
                username:{type:GraphQLString},
                password: {type:GraphQLString}
            },
            resolve:(_,args)=>{
                return child.findOne({username:args.username},{password:1,_id:0})
                .then(child=>{
                    if(args.password===child.password)
                    return true
                })
                .then(result=>{
                    return args.username
                })
                .catch(err => {
                    return err
                })
            }
        },


        userlevel:{
            type: GraphQLList(levels),
            args: {
                username:{type:GraphQLString}
            },
            resolve: (_,args)=>{
                return child.findOne({username:args.username})
                .then(data=>{
                    console.log(data.level)
                    return data.level
                })
                .catch(err=>{
                    console.log('error loading the list')
                })
            }
        },

        all_levels:{
            type: GraphQLList(all),
            resolve:(_,args)=>{
                return quiz.find({})
                .then(data=>{
                    console.log(data)
                    return data
                })
                .catch(err=>{
                    console.log('error')
                })
            }
        },

        quiz_query:{
            type: GraphQLList(quiz_list),
            args:{
                level:{type:GraphQLString}
            },
            resolve(_,args){
                return quiz.findOne({platform:args.level})
                .then(data=>{
                    console.log(data)
                    return data.quiz
                })
            }
        }

       
    })
})

const RootMutationType = new GraphQLObjectType({
    name:'RootMutationType',
    fields: ()=>({
        register:{
            type: GraphQLString,
            args: {
                username:{type:GraphQLString},
                email: {type:GraphQLString},
                password: {type:GraphQLString}
            },
    resolve:(_,args)=>{
        return child.findOne({username:args.username})
            .then(data=>{
                if(!data){
                    const kid = new child({
                        username:args.username,
                        email: args.email,
                        password:args.password
                    })
                    return kid.save()
                }else{
                    return args.username
                }
            })
               .then(data=>{
                    return args.username
                })
                .catch(err => {console.log('error')})
            }
        },

        addlevel:{
            type: GraphQLString,
            args:{
                level: {type: GraphQLString},
                description: {type: GraphQLString}
            },
            resolve:(_,args)=>{
                const level = new quiz({
                    platform:args.level,
                    description:args.description
                })
            return level.save()
                .then(data=>{
                    return args.level+'saved'
                })
                .catch(err=>{console.log(err)})
            }
        },

        addquiz:{
            type: GraphQLString,
            args: {
                quiz:{type: GraphQLString},
                questions:{type: GraphQLString},
                op1: {type: GraphQLString},
                op2: {type: GraphQLString},
                op3: {type: GraphQLString},
                op4: {type: GraphQLString},
                answer:{type: GraphQLString},
                decrip:{type: GraphQLString}
            },
            resolve:(_,args)=>{
                return quiz.findOne({platform:args.quiz})
                .then(data=>{
                    console.log(data)
                data.quiz.push({
                        question:args.questions,
                        option1:args.op1,
                        option2:args.op2,
                        option3:args.op3,
                        option4:args.op4,
                        answer:args.answer,
                        decrip:args.decrip
                    })
                 return data.save()
                })
                .then(result=>{
                    return 'saved'
                })
                .catch(err =>{console.log(err)})
            }
        }
    })
})


const schema = new GraphQLSchema({
    query:RootQueryType,
    mutation:RootMutationType
})

module.exports = schema